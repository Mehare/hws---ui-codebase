$(document).ready(function() {
	$(".btn-field-select").click(function(){
		$(this).parent('.actions-left').addClass('show-popup');	
	});
	$(".btn-accept, .btn-cancel-popup").click(function(){
		$('.actions-left').removeClass('show-popup');
		$('.btn-field-select').removeClass('active');
	});
	 $(".justone").change(function(event){
        if (this.checked){
        $(this).parent('.checkbox-label').parent('.checkbox').parent('.popup-table table tr td').parent('.popup-table table tr').addClass('selected-row');
        } else {
            $(this).parent('.checkbox-label').parent('.checkbox').parent('.popup-table table tr td').parent('.popup-table table tr').removeClass('selected-row');
        }
    });
	 $(".selectall").change(function(event){
		 if (this.checked){
			 $('.popup-table table tr td').parent('.popup-table table tr').addClass('selected-row');
		 }
		 else{
			 $('.popup-table table tr td').parent('.popup-table table tr').removeClass('selected-row');
		 }
	});
	$('.selectall').click(function() {
		if ($(this).is(':checked')) {
			$('.popup-table table tr td .checkbox input:checkbox').prop('checked', true);
		} else {
			$('.popup-table table tr td .checkbox input:checkbox').prop('checked', false);
		}
	});
	
	$(".popup-table table tr td .checkbox input[type='checkbox'].justone").change(function(){
		var a = $(".popup-table table tr td .checkbox input[type='checkbox'].justone");
		if(a.length == a.filter(":checked").length){
			$('.selectall').prop('checked', true);
		}
		else {
			$('.selectall').prop('checked', false);
		}
	});
	
	$('.btn-action').click(function () {
		$('.btn-action.active').removeClass('active');
		$(this).addClass('active');							 
	});
    $('.save-search-btn').click(function () {
        $('.save-search-popup').slideToggle();
    });
$('.collapse_menu').click(function(){
	$('.main-content').toggleClass('main-content-padding');	
	$('.menu--item').removeClass('menu--subitens__opened');
	$('.more-menu').toggle();
});
$('.close-toggle').click(function(){
	$('.vertical_nav').removeClass('vertical_nav__opened');					  
});
$(".vessel-schedule-type").each(function(){
		 $(this).hide();
		if($(this).attr('id') == 'vessel-lbt-1') {
			$(this).show();
		}
	});
	$('.vessel-data-box').on( "click", function(e) {
		$('.vessel-data-box.active').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
		var id = $(this).attr('data-related'); 
		$(".vessel-schedule-type").each(function(){
			$(this).hide();
			if($(this).attr('id') == id) {
				$(this).show();
			}
		});
	});
});